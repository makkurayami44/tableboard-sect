export type TDeficitItem = {
    id: number,
    name: string,
    icon: string,
    caredby: TPlayer,
    subscribed: number,
};

import type {TPlayer} from "@/types/schedule/";
