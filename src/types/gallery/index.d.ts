export type TGalleryImgItem = {
    id: number,
    url: string,
};
