import {AchievementsTypesEnum} from "@/enums/AchievementsTypesEnum";
import {Follower} from "@/types/Follower";

/**
 * Достижения
 */
export namespace Achievements {
    export type TAchievementItem = {
        type: AchievementsTypesEnum;
	id: number;
        name: string;
        gained: number;
        description: string;
        icon: string;
        buttons: null | Array<{
            label: string;
            url: string;
        }>
        checkboxes: null | Array<{
            label: string;
            checked: number;
        }>
        progressbars: null | Array<TProgress>
        dan: null | {
            count: number;
            ending: number;
        };
    };

    export type TProgress = {
        label: string;
        progress: number;
        criteria: number;
    };
    
    export type TRatingItem = {
	place: number;
	progress: number;
	follower: TFollower;
    };
    export type TRating = Array<TRatingItem>;
}
