/**
 * Элемент расписания
 * name - название игры
 * day - день недели
 * date - дата игры
 * time - время начала игры
 * players_max - максимальное кол-во игроков
 * players_left - осталось свободных мест
 * description - описание игры
 * icon - маленькая иконка игры
 * players - игроки, записавшиеся на игру
 * rules - номер группы правил
 * deficit - флаг отображения кнопки "Что мне принести?"
 * rare - флаг редкости мероприятия
 */
export type TScheduleItem = {
    type: number,
    id: number,
    name: string,
    day: string,
    date: string,
    time: string,
    places: number,
    content: string,
    warning: string,
    players: Array<TPlayer>
    description: string,
    unlocking: string,
    subscribed: number,
    icon: string,
    follower: TPlayer,
    deficit: boolean,
    rules: number,
    rare: boolean,
    emoticons?: Array<number>
};

export type TPlayer = {
    id: number,
    name: string,
    surname: string,
    alias: string,
    color: string,
    anchor: string,
    icon: string,
    hint: string,
    gender: number,
};

export type TSchedule = Array<TScheduleItem>;
