export type TQuestItem = {
	id: number;
	title: string;
	description: string;
	until: string;
	crumps: string;
	holiness: string;
	tickets: string;
	subscribed: number;
};