import type {TPlayer} from "@/types/schedule/";
import {PromiseState} from "@/enums/PromiseStateEnum";

export namespace Wishlist {
    /*
 *  owner - подпись владельца вишлиста в нашем обычном формате;
 *  captured - количество забронированных подарков;
 *  new - флаг есть ли новые позиции в этом листе относительно его последнего просмотра.
 */

    export type TItem = {
        owner: TPlayer;
        captured: number;
        new: number;
    };

    /*
     *  owner - подпись владельца вишлиста в нашем обычном формате;
     *  own - флаг - твой ли это вишлист? (возможно это поле тоже можно удалить)
     *  gifts - массив подарков;
     *  hidden - информация о скркытых подарках:
     *      count - количество скрытых подарков;
     *      new - есть ли среди скрытых подарков новые;
     *  comments - массив коментариев.
     */

    export type TItemDetailed = {
        success: boolean;
        owner: TPlayer;
        own: boolean;
        gifts: Array<TGift>;
        hidden: {text: string, new: number};
        comments: Array<TComment>;
    };

    /*
     *  id - идентификатор подарка в листе;
     *  name - наименование подарка О.о;
     *  description - пояснения к подарку - мелкие комменты + возможно ссылки.
     *  captured - флаг - забронирован ли подарок?
     *  new - флаг новая ли это запись относительно последнего просмотра?
     *  editable - флаг показывающий можно ли отображать кнопки "Редактировать" и "Удалить" около этого элемента или нет.
     *  releaseable - флаг показывающий можно ли отображать кнопку "Снять бронь" с этого элемента.
     *  поле capturable не закладывается, вместо этого capturable вычисляется как capturable=!list.own && !gift.captured
     */

    export type TGift = {
        id: number;
        name: string;
        description: string;
        captured: boolean;
        new: number;
        editable: boolean;
        releaseable: boolean;
        obsolete: boolean;
    };

    export type TItemEdit = {
        id: number;
        name?: string;
        description?: string;
        comment?: string;
        type: string;
        author?: TPlayer;
    };

    /*
     *  id - идентификатор коммента в листе;
     *  comment - текст комментария;
     *  author - автор комментария в формате TPlayer;
     *  new - флаг новая ли это запись относительно последнего просмотра?
     *  editable - флаг показывающий можно ли отображать кнопки "Редактировать" и "Удалить" около этого элемента или нет.
     */

    export type TComment = {
        id: number;
        comment: string;
        author: TPlayer;
        new: number;
        editable: boolean;
    };

    export type TAddItemRequest = {
        list: number;
        name?: string;
        description?: string;
        comment?: string;
        anonymous?: boolean;
        id?: number;
    }

    export type TPromiseItem = {
        id: number;
        name: string;
        to: TPlayer;
        state: PromiseState;
    };

    export type TGiftExtra = {
	follower: TPlayer;
        id: number;
        name: string;
        description: string;
        new: number;
        captured: boolean;
    };
}
