export type TResourceItem = {
    id: number,
    name: string,
    icon: string,
    caredby: TPlayer,
    state: number,
};

import type {TPlayer} from "@/types/schedule/";
