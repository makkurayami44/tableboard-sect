/**
 * Сервис авторизации
 */
// @ts-ignore
import Tools from "../helpers/Tools";

export default class AuthService
{
    static token: string = '';
    static tokenName = 'token';
    static useIsAuthorized = false;

    /**
     * Установить токен
     * @param token
     * @param date
     */
    static setToken(token: string, date: Date|null = null): void
    {
        // this.clearData();
        this.token = token;
        this.useIsAuthorized = true;
        Tools.setCookie(this.tokenName, token, date);
    }

    /**
     * Очистить данные
     * @private
     */
    static clearData(): void
    {
        this.setToken( '', null);
    }

    /**
     * Получить из хранилища токен клиента
     */
    static getToken(): string|null
    {
        if (!this.token) {
            this.token = typeof Tools.getCookie(this.tokenName) !== 'undefined'
                ? (Tools.getCookie(this.tokenName) as string)
                : '';
        }
        return this.token;
    }

    static logOut(): void
    {
        this.useIsAuthorized = false;
        this.clearData();
    }

    /**
     * Авторизован ли клиент
     */
    static getUserIsAuthorized(): boolean
    {
        return this.useIsAuthorized;
    }
}
