/**
 * Сервис по определению видимости страницы
 */
import EventApi from "@/api/EventApi";

export class PageVisibilityService {
    private hidden: null | string | undefined = null;
    private visibilityChange: null | string | undefined = null;

    public init(): void {
        if (typeof document.hidden !== 'undefined') {
            this.hidden = 'hidden';
            this.visibilityChange = 'visibilitychange';
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
        } else if (typeof document.msHidden !== 'undefined') {
            this.hidden = 'msHidden';
            this.visibilityChange = 'msvisibilitychange';
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
        } else if (typeof document.webkitHidden !== 'undefined') {
            this.hidden = 'webkitHidden';
            this.visibilityChange = 'webkitvisibilitychange';
        }
        this.initListener();
    }

    private handleVisibilityChange(): void {
        if (!this.hidden) {
            EventApi.triggerEvent(EventApi.SHOW_PAGE_TAB);
        }
    }

    private initListener(): void {
        if (this.visibilityChange) {
            document.addEventListener(this.visibilityChange as string, this.handleVisibilityChange, false);
        }
    }
}

const pageVisibilityService = new PageVisibilityService();

export default pageVisibilityService;
