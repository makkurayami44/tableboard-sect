export default class EventBusService {
    public static readonly INIT_CAROUSEL_WIDGET = 'init_carousel_widget';

    /**
     * Тригер события
     * @param {string} eventName
     * @param data
     */
    public static triggerEvent(eventName: string, data?: any): void {
        if (typeof CustomEvent === 'function') {
            const event = new CustomEvent(eventName, {
                detail: data,
            });
            document.dispatchEvent(event);
        } else {
            const event = document.createEvent('CustomEvent');
            event.initCustomEvent(eventName, false, false, data);
            document.dispatchEvent(event);
        }
    }

    /**
     * Добавление слушателя событий
     * @param eventName
     * @param handler
     */
    public static addEventListener<T = unknown>(
        eventName: string,
        handler: EventListenerOrEventListenerObject
    ): void {
        document.addEventListener(eventName, handler, false);
    }

    /**
     * Удаление слушателя событий
     * @param eventName
     * @param handler
     */
    public static removeEventListener(
        eventName: string,
        handler: EventListenerOrEventListenerObject
    ): void {
        document.removeEventListener(eventName, handler, false);
    }
}
