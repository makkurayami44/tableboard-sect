import {createRouter, createWebHistory} from 'vue-router';
import Schedule from '@/views/schedule/Schedule.vue';
import Breads from '@/views/ratings/Breads.vue';
import Holiness from '@/views/ratings/Holiness.vue';
import Activity from '@/views/ratings/Activity.vue';
import Illness from '@/views/ratings/Illness.vue';
import Tickets from '@/views/ratings/Tickets.vue';
import Authorization from '@/views/authorization/Authorization.vue';
import Achievements from '@/views/achievements/Achievements.vue';
import Deficit from '@/views/deficit/Deficit.vue';
import Wishlists from '@/views/wishlist/Wishlists.vue';
import WishlistDetailed from '@/views/wishlist/WishlistDetailed.vue';
import WishlistsAllActual from '@/views/wishlist/WishlistsAllActual.vue';
import AchievementRating from '@/views/achievements/AchievementRating.vue';
import Tournament from '@/views/tournament/Tournament.vue';
import Promises from '@/views/promises/Promises.vue';
import Resources from '@/views/resources/Resources.vue';
import Game from '@/views/game/Game.vue';
import Console from '@/views/console/Console.vue';
import Quests from '@/views/quests/Quests.vue';
import Rules from '@/views/rules/Rules.vue';
import AuthService from '@/services/AuthService';
import Gallery from "@/views/gallery/Gallery.vue";
import Asylum from "@/views/asylum/Asylum.vue"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'authorization',
      component: Authorization,
      meta: {
        public: true
      }
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: Schedule,
    },
    {
      path: '/breads',
      name: 'breads',
      component: Breads,
    },
    {
      path: '/holiness',
      name: 'holiness',
      component: Holiness,
    },
    {
      path: '/activity',
      name: 'activity',
      component: Activity,
    },
    {
      path: '/tickets',
      name: 'tickets',
      component: Tickets,
    },
    {
      path: '/illness',
      name: 'illness',
      component: Illness,
    },
    {
      path: '/achievements',
      name: 'achievements',
      component: Achievements,
    },
    {
      path: '/achievement_rating',
      name: 'achievement_rating',
      component: AchievementRating,
    },
    {
      path: '/deficit',
      name: 'deficit',
      component: Deficit,
    },
    {
      path: '/tournament',
      name: 'tournament',
      component: Tournament,
    },
    {
      path: '/wishlists',
      name: 'wishlists',
      component: Wishlists,
    },
    {
      path: '/wishlist',
      name: 'wishlist',
      component: WishlistDetailed,
    },
    {
      path: '/wishlists_all_actual',
      name: 'wishlists_all_actual',
      component: WishlistsAllActual,
    },
    {
      path: '/promises',
      name: 'promises',
      component: Promises,
    },
    {
      path: '/game',
      name: 'game',
      component: Game,
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rules,
    },
    {
      path: '/resources',
      name: 'resources',
      component: Resources,
    },
    {
      path: '/console',
      name: 'console',
      component: Console,
    },
    {
      path: '/quests',
      name: 'quests',
      component: Quests,
    },
    {
      path: '/gallery',
      name: 'gallery',
      component: Gallery,
    },
    {
      path: '/gallery-detailed',
      name: 'gallery-detailed',
      component: Gallery,
    },
    {
      path: '/asylum',
      name: 'asylum',
      component: Asylum,
    },
  ],
})

router.beforeEach((to, from, next) => {
  if (!to.meta?.public && !AuthService.getToken()) {
    next({
      path: '/',
    });
    return;
  }

  next();
});

export default router
