import { createApp } from 'vue';
import { createPinia } from 'pinia';

import { Quasar, Notify } from 'quasar';
import quasarLang from 'quasar/lang/ru';

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css';

// Import Quasar css
import 'quasar/src/css/index.sass';

import App from './App.vue';
import router from './router';
import VueAxios from "vue-axios";
import axiosInstance from "@/axios";
// @ts-ignore
import vClickOutside from "click-outside-vue3";

const app = createApp(App);

app.use(vClickOutside);

app.use(Quasar, {
    plugins: {Notify},
    config: {
        notify: {
            color: 'purple-1',
            timeout: 2000,
        },
    },
    lang: quasarLang,
})

app.use(VueAxios, axiosInstance);

app.use(createPinia());
app.use(router);

app.mount('#app');
