export const enum PromiseState {
    BOOKED = 1,
    BOUGHT = 2,
    PACKED = 3,
    GIFTED = 4,
}

export const enum PromiseStateText {
    BOOKED = 'Забронировано',
    BOUGHT = 'Раздобыто',
    PACKED = 'Упаковано',
    GIFTED = 'Подарено',
}

export const enum PromiseActionPrevText {
    CANCEL = 'Отменить бронь',
    BOOK = 'Нет, ешё не раздобыто',
    BUY = 'Нет, ешё не упаковано',
    PACK = 'Нет, ещё не подарено',
}

export const enum PromiseActionNextText {
    BUY = 'Отметить раздобытым',
    PACK = 'Отметить упакованным',
    GIFTED = 'Отметить подаренным',
}
