export const enum EventsTypesEnum {
    GAME_EVENT_TYPE = 1,
    BIRTHDAY_TYPE = 2,
    NOTICE_TYPE = 3,
    SEPARATOR_TYPE = 4,
    CATCH_ME_TYPE = 5,
    ACHIEVEMENT_TYPE = 6,
}
