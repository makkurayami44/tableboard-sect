export const enum SubscriptionStatusEnum {
    NOT_SUBSCRIBED = 0,
    SUBSCRIBED = 1,
    SUBSCRIPTION_IS_DELAYED = 2,
    SUBSCRIPTION_IS_LOCKED = 3,
    NOT_AVAILABLE = 9,
}

export const enum SubscriptionButtonTextEnum {
    SUBSCRIBE = 'Записаться',
    UNSUBSCRIBE = 'Отписаться',
    SUBSCRIPTION_IS_DELAYED = 'Запись откроется ',
    SUBSCRIPTION_IS_LOCKED = 'Запись пока недоступна',
}

export const enum EventContentTypesTextEnum {
    COOPERATIVE = 'Настольная игра, кооперативная',
    COMPETITIVE = 'Настольная игра, соревновательная',
    AGGRESSIVE = 'Настольная игра, агрессивная',
    ASYMMETRIC = 'Настольная игра, асимметричная',
    VIDEOGAME = 'Видеоигра',
    MOVIE = 'Киновечер',
    STREAM = 'Стрим',
}
