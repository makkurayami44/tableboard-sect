/**
 * Хелперы
 */

export default class Tools
{
    /**
     * Записать куку
     * @param name
     * @param date
     * @param value
     */
    public static setCookie(name: string, value: string, date: Date|null): void
    {
        try {
            if (date) {
                document.cookie = `${name}=${value}; path=/; expires="${date.toUTCString()};"`;
            } else {
                document.cookie = `${name}=${value}; path=/`;
            }
        } catch (e) {}
    }

    /**
     * Значение куки по имени
     * @param name
     */
    public static getCookie(name: string): string|undefined
    {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length == 2) {
            return (parts.pop() as string).split(";").shift();
        }
        return undefined;
    }

    public static parseHtml(text: string): string
    {
        const urlRegex = /(https?:\/\/[^\s]+)/g;
        return text.replace(urlRegex, function(url) {
            return '<a href="' + url + '" target="_blank">' + url + '</a>';
        });
    }
}
