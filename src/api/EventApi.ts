/**
 * Апи для работы с событиями
 */
export default class EventApi
{

    static readonly LOGOUT_EVENT = 'logout_event';
    static readonly AUTH_STATUS_CHANGED = 'auth_status_changed';
    static readonly COLLAPSE_EVENTS = 'collapse_events';
    static readonly SHOW_PAGE_TAB = 'show_page_tab';

    public static readonly CHANGE_FIELD = 'change_field';

    /**
     * Тригер события
     * @param {string} eventName
     * @param data
     */
    public static triggerEvent(eventName: string, data?: any): void
    {
        if (typeof CustomEvent === 'function') {
            let event = new CustomEvent(eventName, {
                detail: data
            });
            document.dispatchEvent(event);
        } else {
            let event = document.createEvent('CustomEvent');
            event.initCustomEvent(eventName, false, false, data);
            document.dispatchEvent(event);
        }
    }

    /**
     * Добавление слушателя событий
     * @param eventName
     * @param handler
     */
    public static addEventListener(eventName: string, handler: EventListenerOrEventListenerObject): void
    {
        document.addEventListener(eventName, handler, false);
    }

    /**
     * Удаление слушателя событий
     * @param eventName
     * @param handler
     */
    public static removeEventListener(eventName: string, handler: EventListenerOrEventListenerObject): void
    {
        document.removeEventListener(eventName, handler, false);
    }
}
