// @ts-ignore
import EventApi from "./EventApi";
// @ts-ignore
import {AxiosError} from "axios";
// @ts-ignore
import AuthService from "../services/AuthService";

/**
 * Апи для обработки ошибок
 */
export default class ErrorApi
{
    public readonly ERROR_UNKNOWN = 100;

    public static readonly HTTP_CODE_UNAUTHORIZED = 401;

    public static readonly EVENT_UNAUTHORIZED_ERROR = 'unauthorized_error';

    /**
     * Показать ошибку сервера
     */
    public static showServerError(errorMessage: string | null = null, description: string = ''): void
    {
        alert(errorMessage);
    }

    /**
     * Ошибка авторизации в приватных роутах
     */
    public static unAuthorizedErrorInPrivateRoute(message: string = ''): void
    {
        EventApi.triggerEvent(this.EVENT_UNAUTHORIZED_ERROR, message);
    }

    /**
     * Обработать ошибки
     * @param error
     */
    public static processErrors(error: AxiosError): void {
        if (typeof error.response === 'undefined') {
            ErrorApi.showServerError('Неизвестная ошибка');
            return;
        }

        const { status } = error.response;

        if (status === ErrorApi.HTTP_CODE_UNAUTHORIZED) {
            ErrorApi.unAuthorizedErrorInPrivateRoute('Ошибка авторизации');
            AuthService.logOut();
            return;
        }

        const errorMessage = error.response.data?.error
            || error.response.data?.message
            || `Произошла ошибка\n${error}`;

        ErrorApi.showServerError(errorMessage);
    }
}
