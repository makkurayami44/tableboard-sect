import {defineStore} from 'pinia';
import axiosInstance from "@/axios";
import type { TGalleryImgItem } from "@/types/gallery";

export type RootState = {
	IsMobile: boolean;
	MenuQuests: boolean,
	MenuTournament: boolean,
	MenuGame: boolean,
	MenuResources: boolean,
	MenuConsole: boolean,
	MenuQuestsNew: boolean,
	MenuWishlistNew: boolean,
	MenuTournamentNew: boolean,
	PhotoCarouselIsVisible: boolean,
	PhotoCarouselImages: null | Array<TGalleryImgItem>,
	PhotoCarouselActiveSlide: number;
};
export const GetGlobalsPointer = defineStore({
	id: 'globals',
	state: () => ({
		IsMobile: false,
		//
		MenuQuests: false,
		MenuTournament: false,
		MenuGame: false,
		MenuResources: false,
		MenuConsole: false,
		//
		MenuQuestsNew: false,
		MenuWishlistNew: false,
		MenuTournamentNew: false,
		//
		PhotoCarouselIsVisible: false,
		PhotoCarouselImages: [],
		PhotoCarouselActiveSlide: 0,
	}) as RootState,
	actions: {
		OnLogOut()
		{
			this.MenuGame=false;
			this.MenuResources=false;
			this.MenuConsole=false;
			this.MenuQuests=false;
			this.MenuTournament=false;
			this.MenuWishlistNew=false;
			this.MenuTournamentNew=false;
			this.MenuQuestsNew=false;
		},
		OnLogIn()
		{
			axiosInstance.get(`/menu_extensions`).then(data => {
				this.MenuTournament=data.data.tournament;
				this.MenuGame=data.data.game;
				this.MenuResources=data.data.resources;
				this.MenuConsole=data.data.console;
				this.MenuQuests=data.data.quests;
				//
				this.MenuWishlistNew=data.data.wishlist_new;
				this.MenuTournamentNew=data.data.tournament_new;
				this.MenuQuestsNew=data.data.quests_new;
			});
		},
		OnStartup()
		{
			OnWindowResize();
			window.onresize=OnWindowResize;
		},
		showPhotoCarousel()
		{
			this.PhotoCarouselIsVisible = true;
		},
		hidePhotoCarousel()
		{
			this.PhotoCarouselIsVisible = false;
		},
		setPhotoCarouselActiveSlide(index: number)
		{
			this.PhotoCarouselActiveSlide = index;
		},
		setPhotoCarouselImages(payload: Array<TGalleryImgItem> | null )
		{
			this.PhotoCarouselImages = payload;
		}
	},
	getters: {
		photoCarouselIsVisible: (state): boolean => {
			return state.PhotoCarouselIsVisible;
		},
		getCarouselImages: (state): null | Array<TGalleryImgItem> => {
			return state.PhotoCarouselImages;
		},
		getPhotoCarouselActiveSlide: (state): number => {
			return state.PhotoCarouselActiveSlide;
		},
	}
})

function OnWindowResize(){
	GetGlobalsPointer().IsMobile = window.innerWidth <= 768;
}
