// @ts-ignore
import axios, {AxiosInstance} from 'axios';
// @ts-ignore
import AuthService from "../services/AuthService";
// @ts-ignore
import ErrorApi from "../api/ErrorApi";

const axiosInstance: AxiosInstance = axios.create({
    baseURL: 'http://178.140.0.173/api/',
//  baseURL: 'http://'.concat(window.location.host).concat('/api/') //Определяет по адресной строке к какому из серверов подключаться.
});


axiosInstance.interceptors.request.use((config) => {
    config.params = config.params || {};
    const token = AuthService.getToken();
    if (token && config.headers) {
        config.headers['Authorization'] = `Bearer ${token}`
    }
    return config;
});

axiosInstance.interceptors.response.use((response) => {
    return response;
}, (err) => {
    ErrorApi.processErrors(err);

    return err;
});

export default axiosInstance;
